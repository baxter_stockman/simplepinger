import * as cors from 'cors';
import { NextFunction, Request, Response, Router } from 'express';
import { authenticate } from 'passport';

import { addSites,
         getSites,
         signin,
         signup } from '../controllers/authentication';
import '../utils/passport';

const router = Router();
const requireAuth = authenticate('jwt', { session: false });
const requireSignin = authenticate('local', { session: false });

const whitelist = ['http://localhost:4200'];
const corsOptions = {
  origin: (origin: string, callback: any) => {
    const originIsWhitelisted = whitelist.indexOf(origin) !== -1;
    callback(originIsWhitelisted ? null : 'Bad Request', originIsWhitelisted);
  },
};

router.get('/', (req: Request, res: Response) => {
  // TODO serve react app here, with routing if possible
  res.send('ok');
});

// handle trough special signup handler
router.post('/signup', signup);

// authenticate with local strategy and then send token
router.post('/signin', requireSignin, signin);

// authenticate with jwt strategy and then send site list
router.get('/sites', requireAuth, getSites);

router.options('/sites', cors(corsOptions), requireAuth, getSites);

// TODO CORS doesn't work with GET requests find out why
// router.get('/cors', cors(corsOptions), (req, res) => {
//     res.json({Hello: 'World'});
// });

router.post('/sites', requireAuth, addSites);

export default router;
