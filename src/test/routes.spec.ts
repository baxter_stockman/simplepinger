import * as faker from 'faker';
import 'jest';
import * as mongoose from 'mongoose';

import * as request from 'supertest';

import { app } from '../app';
import { ISite } from '../models/iuser';

describe('routes', () => {
  it('should return 200 OK on GET /', (done: jest.DoneCallback) => {
    const promise = new Promise((resolve, reject) => 42);

    (expect(promise) as any).resolves.toBe(42);
  });
});
    // expect(request(app).get('/'))
    // .then((response) => {
    //   expect(response).toHaveProperty('status', 200);
    //   done();
    // }));
  // it('should be unauthorized to GET /sites without token', () =>
  //   request(app).get('/sites').should.be.rejectedWith(Error, 'Unauthorized'));

  // it('should be unauthorized to POST to /sites without token', () =>
  //   request(app).post('/sites').should.be.rejectedWith(Error, 'Unauthorized'));

  // it('should be Bad Request on invalid POST /signin', () =>
  //   request(app).post('/signin').should.be.rejectedWith(Error, 'Bad Request'));

  // it('should be Bad Request on invalid POST /signup', () =>
  //   request(app).post('/signup').should.be.rejectedWith(Error, 'Internal Server Error'));
