import * as faker from 'faker';
import * as http from 'http';
import * as https from 'https';
import * as moment from 'moment';
import * as mongoose from 'mongoose';

import { ISite, IUserSchema } from '../models/iuser';

import { User } from '../models/user';
import { EmailEmmiter, fetchAllUsersFromDB } from '../utils/emailEmmiter';

const generateUserInfo = (numUsers: number) => {
    return [...Array(numUsers).keys()].map(() => ({
        email: faker.internet.email(),
        password: faker.internet.password(8),
        sites: [...Array(faker.random.number({
             min: 2, max: 10 })).keys()].map(() => ({
            url: faker.internet.url(),
            interval: faker.random.number({ min: 1, max: 5 }),
            online: Math.random() < 0.5 ? true : false,
        })),
    }));
};

const users = generateUserInfo(10);

const asyncTest = async () => {
    try {
        await mongoose.connect('mongodb://localhost/test');
        await mongoose.connection.dropDatabase();
        await Promise.all(
            users.map(async ({ email, password, sites }) => {
                const completeSites = sites;
                const newUser = new User({ email, password, sites: completeSites });
                await newUser.validate();
                return newUser.save();
            }));
        return await mongoose.connection.close();
    } catch (err) {
        console.error(err);
    }
};

asyncTest().then(async () => {
    await mongoose.connect('mongodb://localhost/test');
    console.log('fill db wiith dummy data');
    const dummyUsers = await fetchAllUsersFromDB(User);
    const emailEmmiter = new EmailEmmiter(dummyUsers);
    emailEmmiter.startWatching(1 * 60 * 1000);
    emailEmmiter.on('onlineChange', (v: any) => console.log(v));
    // return await mongoose.connection.close();
});


