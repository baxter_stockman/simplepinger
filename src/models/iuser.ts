import * as mongoose from 'mongoose';

export interface IResult {
    err: any;
    isMatch?: boolean;
}

export interface ISite {
    url: string;
    interval: number;
    online?: boolean;
}

export interface IUserSchema extends mongoose.Document {
    email: string;
    password: string;
    sites: ISite[];
    comparePasswords: (submittedPassword: string) => IResult;
}

