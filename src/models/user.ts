import { compare, genSalt, hash } from 'bcrypt-nodejs';
import * as mongoose from 'mongoose';
import { IResult } from './iuser';

import { NO_OP, SALT_CYCLES } from '../utils/appconfig';

(mongoose as any).Promise = global.Promise;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: true,
        trim: true,
    },
    sites: [{
        url: { // TODO figure out how to make sites mandatory
            type: String,
            required: true,
            trim: true,
            unique: true,
            lowercase: true,
        },
        interval: {
            type: Number,
            required: true,
            min: [1, 'Refresh Interval must be at least 1 minute long'],
        },
        online: {
            type: Boolean,
            required: true,
        },
    }],
});

const asyncGenSalt = (times: number): Promise<string | Error> => {

    return new Promise<string | Error>((resolve, reject) => {
        genSalt(times, (err: Error, salt: string) => err ? reject(err) : resolve(salt));
    });

};

const asyncBcryptHash = (password: string, salt: string):
    Promise<string | Error> => {

    return new Promise<string | Error>((resolve, reject) => {
        hash(password, salt, NO_OP, (err: Error, hashedPassword: string) =>
            err ? reject(err) : resolve(hashedPassword));
    });

};

const asyncComparePasswords = (candidatePassword: string, password: string):
    Promise<boolean | Error> => {

    return new Promise<boolean | Error>((resolve, reject) => {
        compare(candidatePassword, password, (err: Error, isMatch: boolean) =>
            err ? reject(err) : resolve(isMatch));
    });

};

// tslint:disable-next-line:only-arrow-functions
userSchema.pre('save', async function(next) {
    const user = this;
    try {
        const salt = await asyncGenSalt(SALT_CYCLES) as string;
        const hashedPassword = await asyncBcryptHash(user.password, salt);
        user.password = hashedPassword;
        next();
    } catch (err) {
        next(err);
    }
});

// tslint:disable-next-line:only-arrow-functions
userSchema.methods.comparePasswords = async function(submittedPassword: string) {
    const user = this;
    try {
        const isMatch = await asyncComparePasswords(submittedPassword, user.password);
        return { err: null, isMatch } as IResult;
    } catch (err) {
        return { err } as IResult;
    }
};

const user = mongoose.model('user', userSchema);

export { user as User };
