const env = process.env.NODE_ENV || 'development';

const SECRET = (env === 'development') ?
               process.env.DEV_SECRET_KEY :
// tslint:disable-next-line:no-var-requires
               require('../secret.js');

export { SECRET };
