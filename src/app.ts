import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import { config } from 'dotenv';
config();
import * as express from 'express';
// import * as connectLiveReload from 'connect-livereload';
import { Request, Response } from 'express';
import * as session from 'express-session';
import { createServer } from 'http';
import * as mongoose from 'mongoose';
import * as morgan from 'morgan';
import { createTransport } from 'nodemailer';
import { resolve } from 'path';

import router from './routes/routes';

const PORT = process.env.PORT || 3000;
const app = express();
const server = createServer(app);

mongoose.connect('mongodb://localhost/test');

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
//    app.use(connectLiveReload({ port: 35729 }));
  } else if (process.env.NODE_ENV === 'production') {
    app.use(compression());
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(resolve(__dirname, 'public')));

app.use('/', router);

if (require.main === module) {
  server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
}

export { app };
