import * as passport from 'passport';
import {
  ExtractJwt,
  Strategy as JwtStrategy,
  VerifiedCallback,
} from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';

import { IResult, IUserSchema } from '../models/iuser';
import { User } from '../models/user';
import { SECRET } from '../secret';

const localOptions = { usernameField: 'email' };

const localLogin = new LocalStrategy(localOptions,
  async (email: string, candidatePassword: string, done: VerifiedCallback): Promise<void> => {

    try {
      const user = await User.findOne({ email: email.toLowerCase() }) as IUserSchema;
      if (!user) {
        return done(null, false);
      }
      const result = await user.comparePasswords(candidatePassword);
      if (result.err) {
        return done(result.err);
      }
      return done(null, result.isMatch ? user : false);
    } catch (err) {
      return done(err);
    }
  });

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: SECRET,
};

const jwtLogin = new JwtStrategy(jwtOptions,
  async (payload: any, done: VerifiedCallback): Promise<void> => {
    try {
      const user = await User.findById(payload.sub);
      return done(null, user ? user : false);
    } catch (err) {
      return done(err, false);
    }
  });

passport.use(localLogin);
passport.use(jwtLogin);
