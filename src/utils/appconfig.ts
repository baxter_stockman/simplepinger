// tslint:disable-next-line:max-line-length
export const VALID_URL = '^(http://www.|https://www.|http://|https://)[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$';
export const NO_OP = () => { return; };
export const SALT_CYCLES = 10;
