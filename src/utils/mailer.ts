import * as moment from 'moment';
import * as nodemailer from 'nodemailer';

export const mailer = (email: string, siteUrl: string, status: (number | Error)) => {
    const transporter = nodemailer.createTransport({
        port: 1025,
        ignoreTLS: true,
    });
    const currentTime = moment().format('dddd, MMMM Do YYYY, h:mm:ss a');

    const mailOptions = {
        from: '"Pinger Admin" <admin@pingerexample.com>', // sender address
        to: `${email}`, // list of receivers
        subject: 'Server Status', // Subject line
        text: `At ${currentTime} server stoped working and respondend
               with: ${status}`, // plain text body
        html: `<p>At ${currentTime} server stoped working and respondend
               with: ${status}</p>`, // html body
    };

    transporter.sendMail(mailOptions,
        (error: Error, info: nodemailer.SentMessageInfo) => {
            if (error) {
                return console.error('Error sending email: ', error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });
};
