import { EventEmitter } from 'events';
import * as moment from 'moment';
import { Document, Model } from 'mongoose';
import * as request from 'request';
import { mailer } from './mailer';

import { ISite, IUserSchema } from '../models/iuser';
import { User } from '../models/user';

export interface IUrlInfo {
    interval: number;
    online?: boolean;
    lastCheckedAt?: moment.Moment;
}
// TODO define proper types as interfaces
export type SitesMap = Map<string, { interval: number, online: boolean }>;
export type SitesStatusMap = Map<string, { timestamp: Date, sites: SitesMap }>;
export type State_t = Map<string, Map<string, IUrlInfo>>;

export const SITE_CHECK_INTERVAL = 30 * 1000;
export const REQUEST_TIMEOUT = 5 * 1000;

export const fetchAllUsersFromDB = async (userModel: Model<Document>):
    Promise<IUserSchema[]> => {
    try {
        const users = await userModel.find({}) as IUserSchema[];
        return users;
    } catch (e) {
        return e;
    }
};

class EmailEmmiter extends EventEmitter {
    private userMap: State_t;
    private intervalId: NodeJS.Timer;

    constructor(private users: IUserSchema[]) {
        super();
        this.userMap = users.reduce((stateMap, user) => {
            const siteMap = this.makeSiteMap(user);
            return stateMap.set(user.email, siteMap);
        }, new Map() as State_t);
    }

    public startWatching(refreshTimeout: number) {
        this.intervalId = setInterval(() => {
            this.userMap.forEach((sites, email) => {
                sites.forEach(async (urlInfo, url) => {
                    const currentTimeStr = moment().second(0).millisecond(0).toISOString();
                    const urlTimeStr
                        = urlInfo.lastCheckedAt.add(urlInfo.interval, 'm').toISOString();
                    // console.log('currentTime = ', currentTimeStr);
                    // console.log('last checked at = ', urlInfo.lastCheckedAt.toISOString());
                    // console.log('interval = ', urlInfo.interval);
                    // console.log('checkTime = ', urlTimeStr);
                    if (currentTimeStr === urlTimeStr) {
                        const newOnlineStatus = await this.siteStatus(url) === 200;
                        if (urlInfo.online !== newOnlineStatus) {
                            urlInfo = {
                                ...urlInfo,
                                online: newOnlineStatus,
                                lastCheckedAt: moment().second(0).millisecond(0),
                            };
                            this.emit('onlineChange', {
                                email,
                                url,
                                siteInfo: urlInfo,
                            });
                        }
                    }
                });
            });
        }, refreshTimeout);
    }

    public stopWatching() {
        if (this.intervalId) {
            clearInterval(this.intervalId);
        }
    }

    public addUserFromDB(user: IUserSchema) {
        this.userMap.set(user.email, this.makeSiteMap(user));
        return this;
    }

    public async addToWatchList(email: string, sites: ISite[]) {
        const siteList = await Promise.all(
            sites.map(async ({ url, interval, online }) => {
                return {
                    url,
                    interval,
                    online: await this.siteStatus(url) === 200,
                    lastCheckedAt: moment().seconds(0).milliseconds(0),
                };
            },
            ));
        this.userMap.set(email, siteList.reduce(
            (map, { url, interval, online, lastCheckedAt }) => {
                return map.set(url, {
                    interval,
                    online,
                    lastCheckedAt,
                } as IUrlInfo);
            }, new Map<string, IUrlInfo>()));
        return this;
    }

    private makeSiteMap(user: IUserSchema) {
        return user.sites.reduce((map, { url, interval, online }) => {
            return map.set(url, {
                interval,
                online,
                lastCheckedAt: moment(user._id.getTimestamp()).seconds(0).milliseconds(0),
            } as IUrlInfo);
        }, new Map<string, IUrlInfo>());
    }

    private siteStatus(url: string): Promise<number> {
        return new Promise<number | null>((resolve, reject) => {
            request({ url, timeout: REQUEST_TIMEOUT },
                (err: any, res: request.RequestResponse) => {
                    if (err && err.code === 'ETIMEDOUT' && err.connect === true) {
                        // OS TCP teardown timeout, nothing can be done here
                        // console.log(err.connection);
                    }
                    return resolve(res ? res.statusCode : null);
                });
        });
    }
}

export { EmailEmmiter };
