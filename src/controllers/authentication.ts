import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jwt-simple';
import { Document } from 'mongoose';

import { ISite, IUserSchema } from '../models/iuser';

import { User } from '../models/user';
import { SECRET } from '../secret';
import { VALID_URL } from '../utils/appconfig';
import { EmailEmmiter } from '../utils/emailEmmiter';

const genUserToken = (user: any): string =>
  jwt.encode({ sub: user.id, iat: Date.now() }, SECRET);

export const signup = async (req: Request, res: Response,
                             next: NextFunction): Promise<void | Response> => {
  const email = req.body.email;
  const password = req.body.password;

  if (!(email && password)) {
    return next('Email or password missing');
  }

  try {
    const user = await User.findOne({ email }) as IUserSchema;
    if (user) {
      res.status(422).send('Sorry, email is used');
    } else {
      const newUser = new User({ email, password });
      // TODO add proper valiation using Mongoose validation API
      await newUser.validate();
      const success = await newUser.save();
      return success ? res.json({ token: genUserToken(newUser) })
        : next('User could not be saved');
    }

  } catch (err) {
    return next(err);
  }
};

export const signin = (req: Request, res: Response): Response =>
  res.send({ token: genUserToken(req.user) });

export const getSites = async (req: Request, res: Response,
                               next: NextFunction): Promise<void | Response> => {
  const id = req.user.id;
  try {
    const user = await User.findById(id) as IUserSchema;
    return user ? res.status(200).json({ sites: user.sites })
      : next('User could not be found');
  } catch (err) {
    return next(err);
  }
};

const parseInterval = (value: any): number => {
  if (/^[0-9]+$/.test(value)) {
    const interval = Number.parseInt(value, 10);
    if (interval >= 1 && interval < Number.MAX_SAFE_INTEGER) {
      return interval;
    }
  }
  return NaN;
};

const isSiteList = (obj: any): obj is ISite[] => {
  if (!Array.isArray(obj)) {
    return false;
  }
  const validUrl = new RegExp(VALID_URL);
  for (const value of (obj as any[])) {
    const url = value.url;
    const interval = parseInterval(value.interval);
    if (!validUrl.test(url) || Number.isNaN(interval)) {
      return false;
    }
  }
  return true;
};

export const addSites = async (req: Request, res: Response,
                               next: NextFunction): Promise<void | Response> => {
  const clientSiteList: ISite[] = req.body.sites;
  try {
    if (isSiteList(clientSiteList)) {
      const { id, email } = req.user;
      // update site status before saving
      const updatedUser = await User.findByIdAndUpdate(
        id, { sites: clientSiteList },
        { new: true },
        ) as IUserSchema;
      // state update here using event emmiter
      return updatedUser ? res.send('ok') : next('User could not be saved');
    }
    return next(`Invalid site list format. Must be a valid URL
                 and a update interval of at least 1 minute`);
  } catch (err) {
    return next(err);
  }
};
