module.exports = {
  "extends": ["eslint:recommended", "google"],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
    }
  },
  "rules": {
        "no-undef": "off",
        "no-unused-vars": "warn",
        "object-curly-spacing": ["warn", "always"],
        "quotes": ["warn", "single", { "allowTemplateLiterals": true }],
        "brace-style": ["error", "1tbs", { "allowSingleLine": true }],
        "semi": ["error", "always", { "omitLastInOneLineBlock": true}],
        "require-jsdoc": "off",
        "no-console": "off",
    },
};
